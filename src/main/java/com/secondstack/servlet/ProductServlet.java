/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secondstack.servlet;

import com.secondstack.dao.ProductDAO;
import com.secondstack.dao.impl.ProductDAOImpl;
import com.secondstack.db.DatabaseConnection;
import com.secondstack.model.Product;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author jasoet
 */
public class ProductServlet extends HttpServlet {

    /*
     * Get :
     *
     * == 1. List Product => action=list (default url)
     *
     * == 2. Show Individual Product => action=show
     *
     * == 3. Show Create Page => action=create
     *
     * == 4. Show Update Page => action=update
     *
     * == 5. Delete Product => action=delete
     *
     * Post:
     *
     * == 1. Post New Product => action=create (default url)
     *
     * == 2. Post Update Product => action=update
     *
     */
    private String list = "/WEB-INF/pages/product/list.jsp";
    private String show = "/WEB-INF/pages/product/show.jsp";
    private String create = "/WEB-INF/pages/product/create.jsp";
    private String update = "/WEB-INF/pages/product/update.jsp";
    private String error = "/WEB-INF/pages/error.jsp";
    private ProductDAO productDAO;

    public ProductServlet() {
        try {
            productDAO = new ProductDAOImpl(DatabaseConnection.getInstance().getConnection());
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            String action = request.getParameter("action") != null ? request.getParameter("action") : "list";

            if (action.equalsIgnoreCase("show")) {
                String idParam = request.getParameter("id") != null ? request.getParameter("id") : "-1";
                int id = Integer.parseInt(idParam);

                Product data = productDAO.find(id);

                request.setAttribute("data", data);
                RequestDispatcher dispacher = request.getRequestDispatcher(show);
                dispacher.forward(request, response);
            } else if (action.equalsIgnoreCase("create")) {
                RequestDispatcher dispacher = request.getRequestDispatcher(create);
                dispacher.forward(request, response);
            } else if (action.equalsIgnoreCase("update")) {
                RequestDispatcher dispacher = request.getRequestDispatcher(update);
                dispacher.forward(request, response);
            } else if (action.equalsIgnoreCase("delete")) {
                String idParam = request.getParameter("id") != null ? request.getParameter("id") : "-1";
                int id = Integer.parseInt(idParam);

                productDAO.delete(id);

                response.sendRedirect(request.getContextPath() + "/product");
            } else {
                List<Product> data = productDAO.getAll();
                request.setAttribute("data", data);
                RequestDispatcher dispacher = request.getRequestDispatcher(list);
                dispacher.forward(request, response);
            }
        } catch (SQLException ex) {
            request.setAttribute("data", ex);
            RequestDispatcher dispacher = request.getRequestDispatcher(error);
            dispacher.forward(request, response);
        } catch (NumberFormatException ex) {
            request.setAttribute("data", ex);
            RequestDispatcher dispacher = request.getRequestDispatcher(error);
            dispacher.forward(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action") != null ? request.getParameter("action") : "create";

        if (action.equalsIgnoreCase("update")) {
        } else {
        }
    }
}
