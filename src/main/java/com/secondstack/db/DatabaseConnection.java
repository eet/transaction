/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secondstack.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author jasoet
 */
public class DatabaseConnection {

    private Connection connection = null;
    private static DatabaseConnection staticObject = null;
    private String driverClassName = "com.mysql.jdbc.Driver";
    private String url = "jdbc:mysql://localhost:3306/transaction";
    private String username = "root";
    private String password = "localhost";

    public DatabaseConnection() {
        try {

            Class.forName(driverClassName).newInstance();
        } catch (InstantiationException ex) {
            ex.printStackTrace();
        } catch (IllegalAccessException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
    }

    public static DatabaseConnection getInstance() {
        DatabaseConnection conn = null;
        try {
            if (staticObject == null) {
                staticObject = new DatabaseConnection();
                conn = staticObject;
            } else if (staticObject.getConnection().isClosed()) {
                staticObject = new DatabaseConnection();
                conn = staticObject;
            } else {
                conn = staticObject;
            }
            return conn;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public void closeConnection() throws SQLException {
        this.connection.close();
    }

    public Connection getConnection() throws SQLException {
        if (this.connection == null) {
            this.connection = DriverManager.getConnection(url,
                    username, password);
        }
        return this.connection;
    }
}
