/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secondstack.dao;

import com.secondstack.model.Product;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author jasoet
 */
public interface ProductDAO {

    public List<Product> getAll() throws SQLException;

    public Product find(int id) throws SQLException;

    public List<Product> getByName(String name) throws SQLException;

    public int insert(Product d) throws SQLException;

    public int delete(int id) throws SQLException;

    public int update(int id, Product newProduct) throws SQLException;
}
