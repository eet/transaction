

CREATE TABLE product (
                product_id INT AUTO_INCREMENT NOT NULL,
                name VARCHAR(55) NOT NULL,
                description VARCHAR(255) NOT NULL,
                PRIMARY KEY (product_id)
);

CREATE TABLE transaction (
                transaction_id INT AUTO_INCREMENT NOT NULL,
                time DATETIME NOT NULL,
                customer_name VARCHAR(55) NOT NULL,
                customer_address VARCHAR(355) NOT NULL,
                PRIMARY KEY (transaction_id)
);


CREATE TABLE transaction_detail (
                transaction_detail_id INT AUTO_INCREMENT NOT NULL,
                product_id INT NOT NULL,
                transaction_id INT NOT NULL,
                amount INT NOT NULL,
                PRIMARY KEY (transaction_detail_id)
);


ALTER TABLE transaction_detail ADD CONSTRAINT transaction_transaction_detail_fk
FOREIGN KEY (transaction_id)
REFERENCES transaction (transaction_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE transaction_detail ADD CONSTRAINT product_transaction_detail_fk
FOREIGN KEY (product_id)
REFERENCES product (product_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;